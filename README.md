# Merchants welcome

![Bitcoin Accepted Sign](images/bitcoin-accepted.jpeg "Bitcoin Accepted Sign")

This is a write up to help merchants new to cc, or cryptocurrency (such as Bitcoin) begin accepting it for their business.

## 1 - Get a wallet setup

1. Install the super simple phone wallet called [Edge](https://edge.app/). For Android, they have a [direct download link](https://apk.edge.app/) on their website.
	* This [video](https://www.youtube.com/watch?v=78068QgnVJI) shows you how to create an account and get started: https://www.youtube.com/watch?v=78068QgnVJI

1. Make sure to write down any information during wallet creation. Setup the wallets you want to start accepting, such as Bitcoin and BitcoinCash which are commonly used by customers.

1. Optional to setup FIO stuff. ** I have never done this and it outside the scope of this writeup. ** I look forward to learning more about FIO from actual merchants to add this information here. Or feel to contribute to this on Codeberg.org.
	* For your reference, here is a [vid](https://www.youtube.com/watch?v=jclr4EjLU6A) about it.

## 2 - Get a point of sale setup

Here we talk about setting up your point of sale, or POS system. Some merchants might not need this, but we'll cover it and you can make that decision for your own business.

In [this great video](https://www.youtube.com/watch?v=dSC2sgjCg8A), Derrick from AnyPay (which was made in New Hampshire) walks you through how to setup their AnyPay Cryptocurrency POS. I have seen the AnyPay POS in action and must say that it is very impressive.

* Feel free to add me as your AnyPay ambassador with email jahway603 at protonmail dot com. That's written that way to avoid spammers.

In the past I have seen other merchants use different POS, but I recommend AnyPay.
They have also successfully integrated AnyPay with a commercial POS system, so this is possible if your business has those needs.

## 3 - Success

Let's go over the checklist together

* You now have an easy to use and setup wallet
* You have an easy to use and setup point-of-sale (POS)

